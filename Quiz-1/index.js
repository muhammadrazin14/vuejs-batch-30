//Soal 1
var kalimat_1 = "Halo nama saya Muhammad Iqbal Mubarok"
var kalimat_2 = "Saya Iqbal"
var kalimat_3 = "Saya Muhammad Iqbal Mubarok"
//...jawaban soal 1
function jumlah_kata(kata){
    return kata.split(' ').length;
}
console.log(jumlah_kata(kalimat_1))
console.log(jumlah_kata(kalimat_2))
console.log(jumlah_kata(kalimat_3))
console.log(" ")

//Soal 2
//...jawaban soal 2
// Contoh 1
function next_date(tanggal, bulan, tahun){
    var tanggal=1;
    var bulan=3;
    var tahun=2020;

    var bulan_string;
    switch(bulan){
        case 1: 
        bulan_string = "Januari"
        break;

        case 2: 
        bulan_string = "Februari"
        break;

        case 3: 
        bulan_string = "Maret"
        break;
        
        case 4: 
        bulan_string = "April"
        break;

        case 5: 
        bulan_string = "Mei"
        break;

        case 6: 
        bulan_string = "Juni"
        break;

        case 7: 
        bulan_string = "Juli"
        break;

        case 8: 
        bulan_string = "Agustus"
        break;

        case 9: 
        bulan_string = "September"
        break;

        case 10: 
        bulan_string = "Oktober"
        break;

        case 11: 
        bulan_string = "November"
        break;

        case 12: 
        bulan_string = "Desember"
        break;
    }

    return tanggal + " " + bulan_string + " " + tahun;
}
console.log(next_date())

//Contoh 2
function next_date2(tanggal, bulan, tahun){
    var tanggal=1;
    var bulan=3;
    var tahun=2021;

    var bulan_string;
    switch(bulan){
        case 1: 
        bulan_string = "Januari"
        break;

        case 2: 
        bulan_string = "Februari"
        break;

        case 3: 
        bulan_string = "Maret"
        break;
        
        case 4: 
        bulan_string = "April"
        break;

        case 5: 
        bulan_string = "Mei"
        break;

        case 6: 
        bulan_string = "Juni"
        break;

        case 7: 
        bulan_string = "Juli"
        break;

        case 8: 
        bulan_string = "Agustus"
        break;

        case 9: 
        bulan_string = "September"
        break;

        case 10: 
        bulan_string = "Oktober"
        break;

        case 11: 
        bulan_string = "November"
        break;

        case 12: 
        bulan_string = "Desember"
        break;
    }

    return tanggal + " " + bulan_string + " " + tahun;
}
console.log(next_date2())

//Contoh 3
function next_date3(tanggal, bulan, tahun){
    var tanggal=1;
    var bulan=1;
    var tahun=2021;

    var bulan_string;
    switch(bulan){
        case 1: 
        bulan_string = "Januari"
        break;

        case 2: 
        bulan_string = "Februari"
        break;

        case 3: 
        bulan_string = "Maret"
        break;
        
        case 4: 
        bulan_string = "April"
        break;

        case 5: 
        bulan_string = "Mei"
        break;

        case 6: 
        bulan_string = "Juni"
        break;

        case 7: 
        bulan_string = "Juli"
        break;

        case 8: 
        bulan_string = "Agustus"
        break;

        case 9: 
        bulan_string = "September"
        break;

        case 10: 
        bulan_string = "Oktober"
        break;

        case 11: 
        bulan_string = "November"
        break;

        case 12: 
        bulan_string = "Desember"
        break;
    }

    return tanggal + " " + bulan_string + " " + tahun;
}
console.log(next_date3())
