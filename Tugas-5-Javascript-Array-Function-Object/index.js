// soal 1
console.log("jawaban soal 1")
var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];
//... jawaban soal 1
daftarHewan.sort()
for(i=0; i<5; i++){
    console.log(daftarHewan[i] )
}
console.log(" ")



//soal 2
console.log("jawaban soal 2");
//... jawaban soal 2
function introduce(tmplnma, tmplage, tmpladdress, tmplhobby){
    return "Nama saya " + tmplnma + ", umur saya " + tmplage + " tahun" + ", alamat saya di " + tmpladdress + ", dan saya punya hobby yaitu " + tmplhobby;
}
var data = {name : "John" , age : 30 , address : "Jalan Pelesiran" , hobby : "Gaming" }

var perkenalan = introduce(data.name, data.age, data.address, data.hobby);
console.log(perkenalan); // Menampilkan "Nama saya John, umur saya 30 tahun, alamat saya di Jalan Pelesiran, dan saya punya hobby yaitu Gaming" 
console.log(" ");



// soal 3
console.log("jawaban soal 3");
//.. jawaban soal 3
var hitung_1 = "Muhammad";
function hitung_huruf_vokal(){
    return count = hitung_1.match(/[aeiou]/gi).length;
}

var hitung_2 = "Iqbal";
function hitung_huruf_vokal2(){
    return count = hitung_2.match(/[aeiou]/gi).length;
}
console.log(hitung_huruf_vokal(), hitung_huruf_vokal2());
console.log(" ");



//soal 4
console.log("jawaban soal 4");
//.. jawaban soal 4
function hitung0(angka){
    return angka - 2;
}
console.log( hitung0(0) ); // -2


function hitung1(angka){
    return angka - 1;
}
console.log( hitung1(1) ); // 0


function hitung2(angka){
    return angka + 0;
}
console.log( hitung2(2) ); // 2

function hitung3(angka){
    return angka + 1;
}
console.log( hitung3(3) ); // 4

function hitung5(angka){
    return angka + 3;
}
console.log( hitung5(5) ); // 8