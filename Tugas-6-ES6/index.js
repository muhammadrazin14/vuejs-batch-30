    //soal 1
    //.. jawaban soal 1
    const LuasPersegiPanjang = (sisi_a, sisi_b) => {
        return  sisi_a * sisi_b
    }

    console.log("Luas Persegi Panjang: " + LuasPersegiPanjang(7, 4))

    const KelPersegiPanjang = (sisi_a, sisi_b) => {
        var sisi_a=7
        var sisi_b=4
        return  2 * (sisi_a + sisi_b)
    }

    console.log("Keliling Persegi Panjang: " + KelPersegiPanjang())

    //soal 2
    //.. jawaban soal 2
    const newFunction = (firstName, lastName) => {
        return firstName + " " + lastName
    }    

    console.log(newFunction("William", "Imoh"))


    //soal 3
    //.. jawaban soal 3
    var newObject = {
        firstName: "Muhammad",
        lastName: "Iqbal Mubarok",
        address: "Jalan Ranamanyar",
        hobby: "playing football",
    }
    
    const{firstName, lastName, address, hobby} = newObject
    console.log(firstName, lastName, address, hobby)

    //soal 4
    //...jawaban soal 4
    const west = ["Will", "Chris", "Sam", "Holly"]
    const east = ["Gill", "Brian", "Noel", "Maggie"]
    let combined = [west.concat(east)]
    //Driver Code
    console.log(combined)

    //soal 5
    //...jawaban soal 5
    const planet = 'earth'
    const view = 'glass'
    //   var before = 'Lorem ' + view + 'dolor sit amet, ' + 'consectetur adipiscing elit,' + planet 
    const before = {view}
    console.log('Lorem ' + view + 'dolor sit amet, ' + 'consectetur adipiscing elit,' + planet )